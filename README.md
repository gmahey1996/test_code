<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>test</title>
    <link rel="stylesheet" href=".//fontawesome/css/all.min.css">
    <style>
    body{
            margin: 0px; 
    }
    .fix{
            background-color: black;
            height: 100px;
            width: 100%;
            position: fixed;
            margin-top: -20px;
    }
    .logo h3{
            margin-top: 40px;
            margin-left: 100px;
            display: inline-block;
            width: 50%;
            color: orangered;
            position: fixed;
            
    }
    .menu{
        display: inline-block;
        width: 50%;
        word-spacing: 10px;
        font-size: 20px;
        color: wheat;
    }
    ul{
        position: fixed;
        margin-left: 900px;
        margin-top: 25px;
    }
    li{
        display: inline-block;
        
    }
  
    .img{
        background-image: url(".//Images/test.jpg");
        background-size: 100% 60%;
        height: 700px;
        background-repeat: no-repeat;

        
    }
    .btn1{
        color: black;
        border: 2px solid gray;
        padding: 5px;
        margin-left: 100px;
        border: none;
        padding: 5px;
        font-size: 20px;
    }
    .img p{
        text-align: left;
    
    }
    .info p{
        text-align: justify;
        padding-left:80px;
        font-size: 20px;
        width: 80%;
        margin-top: -80px;
    }
    
    #btn2{
        border: 1px solid orangered;
        color: orangered;
        padding: 5px;
        margin-left: 100px;
        margin-top: -400px;
    }
    .content{
        text-align:center;
        margin: 50px 50px;
    }
    .content1{
        float: left;
    }
    .bottom{
        margin: 50px 50px;
        width: 100%;
    }
    .bottom1,.bottom2,.bottom3{
        width: 25%;
        height: 200px;
        float: left;
    }
    /* .bottom1,.bottom2,.bottom3 p{
        text-align: left;    
    } */
    span{
        display: inline-block;
        font-size: 30px;
    }
    .bottom h4{
        margin: 0px;
    }

    
    </style>
</head>
<body>
<div class="main">
    <div class="fix">
    <div  class="logo"> 
        <h3>introspect</h3>
    </div><div class="menu">
         <ul>
           <li>Home</li>
           <li>Generic</li>
           <li>Elements</li>
         </ul>
    </div>
    </div>
 
    <div class="img">
        <h2 style=" padding-top: 180px;padding-left: 100px; color:black;">INTROSPECT: A FREE + FULLY RESPONSIVE <br>
            SITE TEMPLATE BY TEMPLATED</h2>
            <button class="btn1"> Get Started</button>
        
    </div>
    <div class="info">
        <h1 style="margin-top: -300px; padding:80px; color: orangered;">MAGNA ETIAM LOREM</h1>
         <p>Suspendisse mauris. Fusce accumsan mollis eros. Pellentesque a diam sit amet mi ullamcorper vehicula. Integer adipiscin sem. Nullam quis massa sit amet nibh viverra malesuada. Nunc sem lacus, accumsan quis, faucibus non, congue vel, arcu, erisque hendrerit tellus. Integer sagittis. Vivamus a mauris eget arcu gravida tristique. Nunc iaculis mi in ante.</p>
       <button id="btn2">Learn More</button>
    </div>
    <div class="content">
        <div class="content1">
            <h2 style="margin: 0px;">PELLENTESQUE ADIPIS</h2> 
        <img src=".//Images/bg.jpg" height="200px" width="35%" alt="">
        </div>
        <div class="content2">
            <h2 style="margin: 0px;">MORBI INTERDUM MOL</h2>
            <img src=".//Images/bg1.jpg" height="200px" width="25%" alt="">
        </div>
    </div>
    <div class="bottom">
        <div  class="bottom1">
        <span><i class="fas fa-laptop"></i></span>
         <h4 >TEMPUS FEUGIAT</h4>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem impedit excepturi, ex libero fugit odit fugiat quis aperiam sapiente, quae eaque. Expedita, minus blanditiis totam nobis officiis assumenda architecto cum.</p>
    </div>
    <div class="bottom2">
        <span><i class="fas fa-laptop"></i></span>
       <h4 >TEMPUS FEUGIAT</h4>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem impedit excepturi, ex libero fugit odit fugiat quis aperiam sapiente, quae eaque. Expedita, minus blanditiis totam nobis officiis assumenda architecto cum.</p>
    </div>
    <div class="bottom3">
        <span><i class="fas fa-laptop"></i></span>
       <h4>TEMPUS FEUGIAT</h4>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem impedit excepturi, ex libero fugit odit fugiat quis aperiam sapiente, quae eaque. Expedita, minus blanditiis totam nobis officiis assumenda architecto cum.</p>
    </div>
    </div>


</div>
    
    
</body>
</html>